#!/bin/sh
set -e
rm -rf build/pygather
virtualenv build/pygather
./build/pygather/bin/pip install -r requirements.txt
./build/pygather/bin/python generate-site.py
