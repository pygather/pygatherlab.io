Why Gather?
===========

:slug: why-gather
:date: 2017-11-13 17:45
:author: Moshe Zadka

Why Plugins?
------------

Quoting from Mahmoud Hashemi's `excellent post`_ on the matter:

"For all types of software, open-source or otherwise, the scalability of development poses a problem long before scalability of performance and other technical challenges [...] Call them plugins, modules, or extensions, from your browser to your kernel, they are *the* widely successful solution."

.. _excellent post: http://sedimental.org/plugin_systems.html

But really, read the post!

Why Gather?
-----------

Gather is based on the following design ideas:

* The plugin system must be *reusable* -- it should not be the case
  that every system has to have its own plugin system!
* Package creation should be the interface to *creating a plugin*.
  No more (putting a metadata file in some location)
  and no less (just throwing some random Python file on the web
  and tellling people to download it).
* Package installation should be the interface to *activating a plugin*.
  If it's installed in the virtual (or real) environment,
  it's active: there is no "step 2", :code:`pip install` is all it takes.

Gather's Inspirations
---------------------

Pop-psychology books aside, the venusian_ library is good for people
of all genders.
It has a facility for registering plugins,
and discovering them at run-time.
What it lacks is a way to discover a plugin by merely installing it.

`Setuptools entry points`_ allow discovery of installed packages.
However,
most people would like to create their :code:`setup.py`
and forget about it --
not hand-maintain which plugins to register,
as their package grows more and more,
say,
sub-commands.

Both Venusian and Setuptools
have pretty raw ergonomics:
using them for the first time requires making a lot of small decisions.

Enter Gather --
building upon both Venusian and setuptools' entrypoints,
it gives an ergonomic interface to finding plugins --
and a reasonable one for registering them
(though allowing plugin framework authors to wrap it
with something specific to their needs).

.. _Setuptools entry points: https://setuptools.readthedocs.io/en/latest/setuptools.html#dynamic-discovery-of-services-and-plugins

.. _venusian: https://docs.pylonsproject.org/projects/venusian/en/latest/

Use It
------

There is documentation at `ReadTheDocs`_,
it is installable via `PyPI`_
and issues and pull requests are welcome on `GitHub`_.

Using it? Happy about it?
`Let us know`_ and we will be happy to feature you on our blog.

.. _ReadTheDocs: https://gather.readthedocs.io/en/latest/
.. _PyPI: https://pypi.org/project/gather/
.. _GitHub: https://github.com/pygather/gather
.. _Let us know: https://github.com/pygather/blog-ideas/issues
