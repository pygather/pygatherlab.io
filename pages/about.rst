About
-----

:slug: about
:author: Moshe Zadka

Gather is a library that helps find and arrange plugins.
It is intended to be used as plugin infrastructure for any
applications that need "plugins" --
from web frameworks to command-line applications. 
