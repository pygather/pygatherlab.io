import glob
import os
import shutil
import sys

import symplectic
from symplectic import rest

METADATA = symplectic.Metadata(
    title="Gather: A Plugin Framework",
    description="All the news that's fit to Gather",
    links=[("Docs", "https://gather.readthedocs.io/en/latest/")],
)

POSTS = rest.posts_from_rest_files(glob.glob('posts/*.rst'))
PAGES = rest.pages_from_rest_files(glob.glob('pages/*.rst'))
BLOG = symplectic.Blog(metadata=METADATA, posts=POSTS, pages=PAGES)
symplectic.render(BLOG,
                  theme=['bs4blog'],
                  output='public')
shutil.copy('public/river.html', 'public/index.html')
os.mkdir('public/.well-known')
with open('public/.well-known/cf-2fa-verify.txt', 'w') as fpout:
    fpout.write("888ad2302c5ba12")
